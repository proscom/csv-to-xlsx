# csv-to-xlsx

CLI tool to convert csv files to xlsx.

It uses [csv-parse](https://www.npmjs.com/package/csv-parse) and 
[xlsx](https://www.npmjs.com/package/xlsx) under the hood.

Usage:

```bash
$ csv-to-xlsx \
    -i File.csv \
    -o File.xlsx \
    -d "," \
    -e "\"
```
```bash
$ csv-to-xlsx --help
Options:
  --help           Show help                                           [boolean]
  --version        Show version number                                 [boolean]
  --input, -i      Input file                                [string] [required]
  --output, -o     Output file                               [string] [required]
  --delimiter, -d  CSV delimiter character               [string] [default: ","]
  --quote, -q      CSV quote character                   [string] [default: """]
  --escape, -e     CSV escape character                  [string] [default: "\"]
  --trim, -t       Trim whitespaces around delimiter  [boolean] [default: false]
  --sheet, -s      XLSX sheet name                  [string] [default: "import"]
```

