#!/usr/bin/env node

import csvParse from 'csv-parse';
import { readFile } from 'fs';
import { promisify } from 'util';
import * as xlsx from 'xlsx';
import yargs from 'yargs';

const readFilePromise = promisify(readFile);
const csvParsePromise = promisify(csvParse) as (
  input: string,
  options: csvParse.Options
) => any;

const args = yargs.options({
  input: {
    alias: 'i',
    requiresArg: true,
    demandOption: true,
    describe: 'Input file',
    type: 'string',
  },
  output: {
    alias: 'o',
    requiresArg: false,
    demandOption: true,
    describe: 'Output file',
    type: 'string',
  },
  delimiter: {
    alias: 'd',
    requiresArg: true,
    default: ',',
    describe: 'CSV delimiter character',
    type: 'string',
  },
  quote: {
    alias: 'q',
    requiresArg: true,
    default: '"',
    describe: 'CSV quote character',
    type: 'string',
  },
  escape: {
    alias: 'e',
    requiresArg: true,
    default: '\\',
    describe: 'CSV escape character',
    type: 'string',
  },
  trim: {
    alias: 't',
    requiresArg: false,
    default: false,
    describe: 'Trim whitespaces around delimiter',
    type: 'boolean',
  },
  sheet: {
    alias: 's',
    requiresArg: true,
    default: 'import',
    describe: 'XLSX sheet name',
    type: 'string',
  },
}).argv;

interface ConvertOptions {
  input: string;
  output: string;
  delimiter: string;
  quote: string;
  escape: string;
  trim: boolean;
  sheet: string;
}

async function convert(options: ConvertOptions) {
  const csvBodyBuffer = await readFilePromise(options.input);
  const csvBody = csvBodyBuffer.toString();
  const data = await csvParsePromise(csvBody, {
    delimiter: options.delimiter,
    escape: options.escape,
    trim: options.trim,
    quote: options.quote,
  });
  const wb = xlsx.utils.book_new();
  const ws = xlsx.utils.aoa_to_sheet(data);
  xlsx.utils.book_append_sheet(wb, ws, options.sheet);
  xlsx.writeFile(wb, options.output);
}

function getOutputFromInput(input: string) {
  return input + '.xlsx';
}

convert({
  ...(args as ConvertOptions),
  output: args.output || getOutputFromInput(args.input),
}).catch((e) => console.error(e));
